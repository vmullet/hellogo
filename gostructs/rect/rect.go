package main

import "fmt"

type Rect struct {
	Width, Height int
}

// Value receiver because
// it receives a copy of Rect
func (r Rect) Area() int {
	return r.Width * r.Height
}

// To customize print
func (r Rect) String() string {
	return fmt.Sprintf("Rect ==> width=%v, height=%v", r.Width, r.Height)
}

func main() {
	r := Rect{2, 4}
	fmt.Printf("Rect area=%v\n", r.Area())
	fmt.Println(r)
}
