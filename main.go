package main

import (
	"fmt"

	"training.go/hellogo/gostructs/player"
)

var pi = 3.14

func main() {
	var pl player.Player
	pl.Name = "Bob"
	pl.Age = 10

	fmt.Printf("Player 1 : %v\n", pl)
	fmt.Printf("pl.Name=%v, pl.Age=%v\n", pl.Name, pl.Age)
	a := player.Avatar{
		Url: "http://avatar.jpg",
	}
	fmt.Printf("avatar : %v\n", a)

	p3 := player.Player{
		Name: "John",
		Age:  25,
		Avatar: player.Avatar{
			Url: "http://url.com",
		},
	}
	fmt.Printf("Player 3 : %v\n", p3)

	p4 := player.New("Bobette")
	fmt.Printf("Player 4 : %v\n", p4)
}
