package filter

import (
	"github.com/disintegration/imaging"
)

type Filter interface {
	Process(srcPath, dstPath string) error
}

type Grayscale struct{}

func (g Grayscale) Process(srcPath, dstPath string) error {
	src, err := imaging.Open(srcPath)
	if err != nil {
		return err
	}
	img := imaging.Grayscale(src)
	err = imaging.Save(img, dstPath)
	if err != nil {
		return err
	}
	return nil
}

type Blur struct{}

func (b Blur) Process(srcPath, dstPath string) error {
	src, err := imaging.Open(srcPath)
	if err != nil {
		return err
	}
	img := imaging.Blur(src, 3.5)
	err = imaging.Save(img, dstPath)
	if err != nil {
		return err
	}
	return nil
}
