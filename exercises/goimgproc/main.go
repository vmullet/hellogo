package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"training.go/hellogo/exercises/goimgproc/filter"
	"training.go/hellogo/exercises/goimgproc/task"
)

func main() {
	var srcDir = flag.String("src", "", "Input directory")
	var dstDir = flag.String("dst", "", "Output directory")
	var filterType = flag.String("filter", "grayscale", "grayscale/blur")
	var tasktype = flag.String("task", "waitgrp", "Task type")
	var poolsize = flag.Int("poolsize", 4, "Number of workers")
	flag.Parse()

	var f filter.Filter
	switch *filterType {
	case "grayscale":
		f = filter.Grayscale{}
	case "blur":
		f = filter.Blur{}
	default:
		fmt.Printf("Undefined filter type, got=%v", *filterType)
		os.Exit(1)
	}

	var t task.Tasker
	switch *tasktype {
	case "waitgrp":
		t = task.NewWaitGrpTask(*srcDir, *dstDir, f)
	case "channel":
		t = task.NewChanTask(*srcDir, *dstDir, f, *poolsize)
	default:
		fmt.Printf("Undefined task type, got=%v", *tasktype)
		os.Exit(1)
	}

	start := time.Now()
	t.Process()
	elapsed := time.Since(start)
	fmt.Printf("Image processing took %s\n", elapsed)
}
