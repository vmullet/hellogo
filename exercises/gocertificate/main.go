package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"training.go/hellogo/exercises/gocertificate/cert"
	"training.go/hellogo/exercises/gocertificate/html"
	"training.go/hellogo/exercises/gocertificate/pdf"
)

func main() {
	outputType := flag.String("type", "pdf", "Output type of the certificate.")
	course := flag.String("course", "Golang", "Course type of the certificate.")
	student := flag.String("student", "Valentin Mullet", "Student concerned by the certificate.")
	date := time.Now().Format("2006-01-02")
	file := flag.String("file", "", "Input csv file to generate certificates")
	flag.Parse()

	var saver cert.Saver
	var err error
	switch *outputType {
	case "html":
		saver, err = html.New("output")
	case "pdf":
		saver, err = pdf.New("output")
	default:
		fmt.Printf("Unsupported output type=%v, expected pdf or html\n", *outputType)
		os.Exit(1)
	}

	if err != nil {
		fmt.Printf("Could not resolve generator: %v", err)
		os.Exit(1)
	}

	if *file != "" {
		certs, err := cert.ParseCSV(*file)
		if err != nil {
			fmt.Printf("Error while parsing csv file=%v", file)
		}
		for _, c := range certs {
			err = saver.Save(*c)
			if err != nil {
				fmt.Printf("Could not save cert=%v", c)
			}
		}
	} else {
		c, err := cert.New(*course, *student, date)
		if err != nil {
			fmt.Printf("Error during certificate creation: %v", err)
			os.Exit(1)
		}

		err = saver.Save(*c)
		if err != nil {
			fmt.Printf("Could not save cert=%v", c)
		}
	}

}
