package main

import "fmt"

func main() {
	var names [3]string
	fmt.Printf("names=%v (len=%d)\n", names, len(names))

	names[0] = "Bob"
	names[2] = "Alice"
	fmt.Printf("names=%v (len=%v)\n", names, len(names))

	fmt.Printf("names[2]=%s\n", names[2])

	odds := [4]int{1, 3}
	fmt.Printf("odds=%v (len=%v)\n", odds, len(odds))
}
