package main

import (
	"fmt"
	"time"
)

func hello(c chan string) {
	c <- "Hello there!"
	fmt.Println("Hello finished")
	fmt.Println(<-c)
}

func main() {
	c := make(chan string)
	go hello(c)

	fmt.Println(<-c)
	c <- "Message from main"

	time.Sleep(1 * time.Second)
}
