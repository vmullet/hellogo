package main

import (
	"fmt"
	"strings"
)

type Post struct {
	Title     string
	Text      string
	published bool
}

func (p Post) Headline() string {
	return fmt.Sprintf("%v - %v", p.Title, p.Text[:50])
}

func (p *Post) Published() bool { return p.published }

func (p *Post) SetPublished(published bool) {
	p.published = published
}

func UpperTitle(p *Post) {
	p.Title = strings.ToUpper(p.Title)
}

func main() {
	p := Post{
		Title: "Go release",
		Text: `Go is a wonderful programming language,
		Everyone shoud code in Go ;)`,
	}

	fmt.Println(p.Headline())

	fmt.Printf("Post published : %v\n", p.Published())
	p.SetPublished(true)
	fmt.Printf("Post published : %v\n", p.Published())

	pythonPost := &Post{
		Title: "Python Intro",
		Text: `Python is a wonderful language, everyone should
		use python ;)`,
	}

	UpperTitle(pythonPost)
	fmt.Println(pythonPost.Headline())
}
