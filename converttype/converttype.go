package main

import "fmt"

func main() {
	var percentage float64 = 2.0

	fmt.Printf("Current percent %f%%\n", percentage)
	fmt.Printf("Decimal : %d%%\n", int(percentage))

	n := 42
	f := float64(n) + 0.42
	fmt.Printf("float=%f\n", f)
}
