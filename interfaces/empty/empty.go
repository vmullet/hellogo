package main

import "fmt"

type Person struct {
	Name string
	Age  int
}

type Cooker interface {
	Cook()
}

type Chef struct {
	Person
	Stars int
}

func (c Chef) Cook() {
	fmt.Printf("I'm cooking with %v starts\n", c.Stars)
}

func processPerson(i interface{}) {
	switch p := i.(type) {
	case Person:
		fmt.Printf("We have a person=%v\n", p)
	case Chef:
		fmt.Printf("We have a chef=%v\n", p)
	default:
		fmt.Printf("Unknown type=%T, value=%v\n", p, p)
	}
}

func main() {
	var x interface{} = Person{"Bob", 12}
	fmt.Printf("x type=%T, data=%v\n", x, x)

	s, ok := x.(string)
	fmt.Printf("Person as string ok? %v. value=%v\n", ok, s)

	i, ok := x.(int)
	fmt.Printf("Person as string ok? %v. value=%v\n", ok, i)

	processPerson(x)

	x = Chef{
		Stars: 4,
		Person: Person{
			Name: "Alice",
			Age:  22,
		},
	}

	processPerson(x)
	processPerson(3)
	processPerson("John")
}
