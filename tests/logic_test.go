package tests

import "testing"

func TestMult(t *testing.T) {
	total := Mult(2, 3)
	if total != 6 {
		t.Errorf("Incorrect Mult got %d, want=%d", total, 6)
	}
}
