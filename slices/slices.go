package main

import "fmt"

func showSlice(slice []int) {
	fmt.Printf("%v (len=%d, cap=%d)\n", slice, len(slice), cap(slice))
}

func showSliceStr(slice []string) {
	fmt.Printf("%v (len=%d, cap=%d)\n", slice, len(slice), cap(slice))
}

func main() {
	nums := make([]int, 2, 3)
	nums[0] = 10
	nums[1] = -2
	showSlice(nums)

	nums = append(nums, 64)
	showSlice(nums)
	nums = append(nums, -8)
	showSlice(nums)

	letters := []string{"g", "o", "l", "a", "n", "g"}
	letters = append(letters, "!")
	showSliceStr(letters)

	sub1 := letters[:2]
	sub2 := letters[2:]
	showSliceStr(sub1)
	showSliceStr(sub2)

	fmt.Println("---------------------------------")

	sub2[0] = "UP"
	showSliceStr(sub1)
	showSliceStr(sub2)

	subCopy := make([]string, len(sub1))
	copy(subCopy, sub1)
	subCopy[0] = "DOWN"

	showSliceStr(subCopy)
	showSliceStr(sub1)
	showSliceStr(sub2)

}
