# HelloGO

This repo showcases many aspects of the golang language ([official website](https://golang.org)) to learn
it & master it.

Every folder is about one notion (pointers, structs, interfaces, concurrency...) & you can also find
projects / exercices

## List of notions

+ package
+ imports
+ variables
+ types / conversion
+ multiple returns
+ functions
+ arrays / slices / maps
+ range
+ errors management
+ structs
+ default / external libraries
+ concurrency
+ databases
+ defer / panic
+ command line interfaces (CLI)...
+ etc...
  
## List of projects

Projects are located in the folder "exercises".

+ goplace : A project to find & replace words in a text file
+ gohangman : The hangman game in cli
+ gocertificate : A pdf / html certificate generator
+ goimgproc : A image processor app which uses channels / goroutines to improve performance

## LICENCE

This project is licnesed under the MIT License
