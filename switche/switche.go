package main

import (
	"fmt"
)

func main() {
	weekday := 2
	fmt.Printf("Day of the week=%d\n", weekday)

	switch weekday {
	case 1:
		fmt.Println("Beginning of the week, let's get to work!")
	case 3:
		fmt.Println("Wednesday, half is done!")
	default:
		fmt.Println("Nothing special today !")
	}

	hour := 10
	fmt.Printf("Current time=%d\n", hour)
	switch {
	case hour < 12:
		fmt.Println("it's the morning")
	case hour > 12 && hour <= 18:
		fmt.Println("it's the afternoon")
	case hour > 18:
		fmt.Println("it's the evening")
	}
}
