package main

import (
	"fmt"
	"strings"
)

func printInfoNoParam() {
	fmt.Printf("Name=%s, age=%d, email=%s\n", "Bob", 10, "bob@golang.org")
}

func printInfoParams(name string, age int, email string) {
	fmt.Printf("Name=%s, age=%d, email=%s\n", name, age, email)
}

func avg(x, y float64) float64 {
	return (x + y) / 2
}

func sumNamedReturn(x, y, z int) (sum int) {
	sum = x + y + z
	return sum
}

/////////////////////////////////////////////////////////////////

func MultReturn() (int, string) {
	return 4, "hello"
}

func ToLowerStr(name string) (string, int) {
	return strings.ToLower(name), len(name)
}

func main() {
	printInfoNoParam()
	printInfoParams("Alice", 15, "alice@golang.org")
	fmt.Printf("avg %f\n", avg(16.3, 25.0))

	sum := sumNamedReturn(10, 25, 7)
	fmt.Printf("Sum result=%d\n", sum)

	num, name := MultReturn()
	fmt.Printf("%d %s\n", num, name)

	lowerName, len := ToLowerStr("ALICE")
	fmt.Printf("%s (len=%d)\n", lowerName, len)

	_, len = ToLowerStr("BOB")
	fmt.Printf("%s (len=%d)\n", "bob", len)
}
